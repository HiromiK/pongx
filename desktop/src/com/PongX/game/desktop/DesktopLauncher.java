package com.PongX.game.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.PongX.game.PongX;

public class DesktopLauncher {

    public static void main(String[] arg) {
        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.width = PongX.SCREEN_WIDTH;
        config.height = PongX.SCREEN_HEIGHT;
        config.title = "PongX";
        new LwjglApplication(new PongX(), config);
    }
}
