/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.PongX.powers;

import com.PongX.classes.Player;
import com.PongX.classes.Power;
import com.badlogic.gdx.graphics.Color;

/**
 *
 * @author Licence
 */
public class Control extends Power {

    public static final float DURATION = 1f;
    public static final boolean SELF_USE = false;
    public static final int HEIGHT = 20;
    public static final Color COLOR = Color.PINK;
    public static final String NAME = "Control";
    
    int oldKeyUp;
    int oldKeyDown;
    
    public Control() {
        super(DURATION, SELF_USE, COLOR, NAME);
    }


    @Override
    public void effect(Player player) {
    	this.oldKeyDown = player.getKeyDown();
    	this.oldKeyUp = player.getKeyUp();
        player.setKeyDown(caller.getKeyDown());
        player.setKeyUp(caller.getKeyUp());
    }
    

    @Override
    public void removeEffect(Player player) {
        player.setKeyDown(oldKeyDown);
        player.setKeyUp(oldKeyUp);
    }

}
