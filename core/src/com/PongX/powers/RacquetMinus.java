/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.PongX.powers;

import com.PongX.classes.Player;
import com.PongX.classes.Power;
import com.badlogic.gdx.graphics.Color;

/**
 *
 * @author Licence
 */
public class RacquetMinus extends Power {
    public static final float DURATION = 5f;
    public static final boolean SELF_USE = false;
    public static final int HEIGHT = 50;
    public static final Color COLOR = Color.RED;
    public static final String NAME = "RacquetMinus";

    public RacquetMinus() {
        super(DURATION,SELF_USE, COLOR, NAME);
    }
    
    @Override
    public void effect(Player player) {
        player.setHeight(player.getHeight() -  HEIGHT);
        player.setY(player.getY() + (HEIGHT / 2));
    }

    @Override
    public void removeEffect(Player player) {
        checkBoundsAndAdd(player, HEIGHT);
    }

}
