/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.PongX.powers;

import com.PongX.classes.Player;
import com.PongX.classes.Power;
import com.badlogic.gdx.graphics.Color;

/**
 *
 * @author Licence
 */
public class Freeze extends Power {

    public static final float DURATION = 0.5f;
    public static final boolean SELF_USE = false;
    public static final int HEIGHT = 20;
    public static final Color COLOR = Color.CYAN;
    public static final String NAME = "Freeze";

    public Freeze() {
        super(DURATION, SELF_USE, COLOR, NAME);
    }


    @Override
    public void effect(Player player) {
        player.setFrozen(true);
    }
    

    @Override
    public void removeEffect(Player player) {
        player.setFrozen(false);
    }

}
