/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.PongX.powers;

import com.PongX.classes.Player;
import com.PongX.classes.Power;
import com.badlogic.gdx.graphics.Color;

/**
 *
 * @author Licence
 */
public class RacquetPlus extends Power {

    public static final float DURATION = 5f;
    public static final boolean SELF_USE = true;
    public static final int HEIGHT = 20;
    public static final Color COLOR = Color.GREEN;
    public static final String NAME = "RacquetPlus";

    public RacquetPlus() {
        super(DURATION, SELF_USE, COLOR, NAME);
    }


    @Override
    public void effect(Player player) {
        checkBoundsAndAdd(player, HEIGHT);
    }
    

    @Override
    public void removeEffect(Player player) {
        player.setHeight(player.getHeight() - HEIGHT);
        player.setY(player.getY() + (HEIGHT / 2));
    }

}
