/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.PongX.powers;

import com.PongX.classes.Player;
import com.PongX.classes.Power;
import com.badlogic.gdx.graphics.Color;

/**
 *
 * @author Licence
 */
public class InvertKeys extends Power {

    public static final float DURATION = 3f;
    public static final boolean SELF_USE = false;
    public static final int HEIGHT = 20;
    public static final Color COLOR = Color.ORANGE;
    public static final String NAME = "InvertKeys";

    public InvertKeys() {
        super(DURATION, SELF_USE, COLOR, NAME);
    }


    @Override
    public void effect(Player player) {
    	int keyDown = player.getKeyDown();
        player.setKeyDown(player.getKeyUp());
        player.setKeyUp(keyDown);
    }
    

    @Override
    public void removeEffect(Player player) {
    	effect(player);
    }

}
