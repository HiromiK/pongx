package com.PongX.game;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

import com.PongX.classes.Ball;
import com.PongX.classes.Item;
import com.PongX.classes.Player;
import com.PongX.classes.Power;
import com.PongX.classes.SoundHandler;
import com.PongX.powers.Control;
import com.PongX.powers.Freeze;
import com.PongX.powers.InvertKeys;
import com.PongX.powers.RacquetMinus;
import com.PongX.powers.RacquetPlus;
import com.PongX.powers.Slow;
import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.GL20;

public class PongX extends ApplicationAdapter {

    public static final int SCREEN_WIDTH = 800, SCREEN_HEIGHT = 480, SCREEN_GAP = 10;
    public static final int RACQUET_WIDTH = 20, RACQUET_HEIGHT = 125, RACQUET_Y = (SCREEN_HEIGHT / 2) - (RACQUET_HEIGHT / 2), RACQUET_SHIFTING = 400;
    public static final int BALL_XPOSITION = 400, BALL_YPOSITION = 300, BALL_SIZE = 25;
    public static final int BACKGROUNDCOLOR_RED = 0, BACKGROUNDCOLOR_GREEN = 0, BACKGROUNDCOLOR_BLUE = 0, BACKGROUNDCOLOR_ALPHA = 1;
    public static final int ITEM_HEIGHT = 20;
    public static final int PLAYER1_KEYUP = Keys.Z, PLAYER1_KEYDOWN = Keys.S, PLAYER1_KEYACTION = Keys.SPACE;
    public static final int PLAYER2_KEYUP = Keys.UP, PLAYER2_KEYDOWN = Keys.DOWN, PLAYER2_KEYACTION = Keys.ENTER;
    public static final int SCORE_XGAP = 15, SCORE_YGAP = SCREEN_HEIGHT - 50;
    public static final int PLAYER2_SCORE_POSITIONX = (SCREEN_WIDTH / 2) + (SCORE_XGAP);
    public static final int PLAYER1_SCORE_POSITIONX = (SCREEN_WIDTH / 2) - (SCORE_XGAP);
    public static final int POWERNAME_XGAP = 40, POWERNAME_YGAP = 25;
    public static final int PLAYER2_POWERNAME_POSITIONX = SCREEN_WIDTH - (POWERNAME_XGAP + 100);
    public ArrayList<Item> items = new ArrayList<Item>();
    public ArrayList<Power> powers = new ArrayList<Power>(Arrays.asList(new RacquetMinus(), new RacquetPlus(), new Freeze(), new InvertKeys(), new Control(), new Slow()));
    public SoundHandler soundHandler;
    float itemTimer = 6f;
    float currentTime = 0;
    float waitTimer = 3f;
    Player player1;
    Player player2;
    Ball ball;
    boolean pausePlayer1 = true;
    boolean pausePlayer2 = true;
    boolean pauseOverlap = true;
    boolean wait = true;

    @Override
    public void create() {
        soundHandler = new SoundHandler();
        player1 = new Player(SCREEN_GAP, RACQUET_Y, PLAYER1_KEYUP, PLAYER1_KEYDOWN, PLAYER1_SCORE_POSITIONX, SCORE_YGAP, POWERNAME_XGAP, POWERNAME_YGAP);
        player2 = new Player(SCREEN_WIDTH - (SCREEN_GAP + RACQUET_WIDTH), RACQUET_Y, PLAYER2_KEYUP, PLAYER2_KEYDOWN, PLAYER2_SCORE_POSITIONX, SCORE_YGAP, PLAYER2_POWERNAME_POSITIONX, POWERNAME_YGAP);
        ball = new Ball(BALL_SIZE);
    }

    @Override
    public void render() {
        Gdx.gl.glClearColor(BACKGROUNDCOLOR_RED, BACKGROUNDCOLOR_GREEN, BACKGROUNDCOLOR_BLUE, BACKGROUNDCOLOR_ALPHA);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT); //Clear whole screen
        player1.draw();
        player2.draw();
        ball.draw();
        drawItems();
        update();
    }

    public void update() {
        float newY;
        if (wait) {
            waitTimer -= Gdx.graphics.getDeltaTime();
            if (waitTimer <= 0.0f) {
                waitTimer = 3.0f;
                wait = false;
                resetPlayersPosition();
                resetScore();
                soundHandler.playGameStart();
            }
        } else {
            if (!player2.isFrozen()) {
                if (Gdx.input.isKeyPressed(player2.getKeyDown())) {
                    newY = player2.getY() - player2.getRacquetShifting() * Gdx.graphics.getDeltaTime();
                    if (isInRange(newY, 0, SCREEN_HEIGHT - player2.getHeight())) {
                        player2.setY(newY);
                    }
                    checkItemPickUp(player2);
                }
                if (Gdx.input.isKeyPressed(player2.getKeyUp())) {
                    newY = player2.getY() + player2.getRacquetShifting() * Gdx.graphics.getDeltaTime();
                    if (isInRange(newY, 0, SCREEN_HEIGHT - player2.getHeight())) {
                        player2.setY(newY);
                    }
                    checkItemPickUp(player2);
                }
            }
            if (!player1.isFrozen()) {
                if (Gdx.input.isKeyPressed(player1.getKeyDown())) {
                    newY = player1.getY() - player1.getRacquetShifting() * Gdx.graphics.getDeltaTime();
                    if (isInRange(newY, 0, SCREEN_HEIGHT - player1.getHeight())) {
                        player1.setY(newY);
                    }
                    checkItemPickUp(player1);
                }
                if (Gdx.input.isKeyPressed(player1.getKeyUp())) {
                    newY = player1.getY() + player1.getRacquetShifting() * Gdx.graphics.getDeltaTime();
                    if (isInRange(newY, 0, SCREEN_HEIGHT - player1.getHeight())) {
                        player1.setY(newY);
                    }
                    checkItemPickUp(player1);
                }
            }
            if (Gdx.input.isKeyPressed(PLAYER1_KEYACTION)) {
                if (pausePlayer1) {
                    if (!player1.getPowers().isEmpty()) {
                        int i = 0;
                        while (i < player1.getPowers().size() && player1.getPowers().get(i).getActive()) {
                            i++;
                        }
                        if (i != player1.getPowers().size()) {
                            Power power = player1.getPowers().get(i);
                            Player target;
                            if (power.getSelfUse()) {
                                target = player1;
                            } else {
                                target = player2;
                            }
                            power.effect(target);
                            power.setStarted(currentTime);
                            power.setActive(Boolean.TRUE);
                            soundHandler.playItemUse();
                        }
                    }
                    pausePlayer1 = false;
                }
            } else {
                pausePlayer1 = true;
            }
            if (Gdx.input.isKeyPressed(PLAYER2_KEYACTION)) {
                if (pausePlayer2) {
                    if (!player2.getPowers().isEmpty()) {
                        int i = 0;
                        while (i < player2.getPowers().size() && player2.getPowers().get(i).getActive()) {
                            i++;
                        }
                        if (i != player2.getPowers().size()) {
                            Power power = player2.getPowers().get(i);
                            Player target;
                            if (power.getSelfUse()) {
                                target = player2;
                            } else {
                                target = player1;
                            }
                            power.effect(target);
                            power.setStarted(currentTime);
                            power.setActive(Boolean.TRUE);
                            soundHandler.playItemUse();
                        }
                    }
                    pausePlayer2 = false;
                }
            } else {
                pausePlayer2 = true;
            }

            if (ball.overlaps(player1) || ball.overlaps(player2)) {
                if (pauseOverlap) {
                    ball.getVelocity().scl(-1, 1);
                    ball.setAcceleration(ball.getAcceleration() + 0.2f);
                    soundHandler.playPaddleHit();
                    pauseOverlap = false;
                }
            } else {
                pauseOverlap = true;
            }

            if (ball.getY() + ball.getHeight() >= SCREEN_HEIGHT) {
                ball.getVelocity().scl(1, -1);
                ball.setAcceleration(ball.getAcceleration() + 0.2f);
            }
            if (ball.getY() <= 0) {
                ball.getVelocity().scl(1, -1);
                ball.setAcceleration(ball.getAcceleration() + 0.2f);
            }

            if (ball.outOfScreen() != 0) {
                if (ball.outOfScreen() == 1) {
                    player2.setScore(player2.getScore() + 1);
                } else {
                    player1.setScore(player1.getScore() + 1);
                }
                soundHandler.playLosePoint();
                ball.resetPosition();
                ball.setAcceleration(Ball.ACCELERATION);
                ball.velocity.scl(-1, ball.random());
                if (player2.getScore() == 10 || player1.getScore() == 10) {
                    soundHandler.playGameOver();
                    wait = true;
                }
            }

            itemTimer += Gdx.graphics.getDeltaTime();
            currentTime += Gdx.graphics.getDeltaTime();
            checkItemElapsed(currentTime);
            if (itemTimer >= 10f) {
                itemTimer = 0f;
                addItem();
            }
            ball.update(Gdx.graphics.getDeltaTime());
        }
    }

    private void addItem() {
        float itemPositionY;
        float item2PositionY;
        Item item;
        Item item2;
        if (items.size() < 10) {
            do {
                itemPositionY = (int) (Math.random() * (SCREEN_HEIGHT - ITEM_HEIGHT + 1));
                int randomNum = (int) (Math.random() * powers.size());
                Power power = powers.get(randomNum);
                power.setCaller(player1);
                item = new Item(player1.getX(), itemPositionY, RACQUET_WIDTH, ITEM_HEIGHT, power.clone());
            } while ((item.overlaps(player1) || checkIfItemOverlaps(item)));
            do {
                item2PositionY = (int) (Math.random() * (SCREEN_HEIGHT - ITEM_HEIGHT + 1));
                int randomNum = (int) (Math.random() * powers.size());
                Power power = powers.get(randomNum);
                power.setCaller(player2);
                item2 = new Item(player2.getX(), item2PositionY, RACQUET_WIDTH, ITEM_HEIGHT, power.clone());
            } while ((item2.overlaps(player2) || checkIfItemOverlaps(item2)));
            items.add(item);
            items.add(item2);
        }
    }

    public void checkItemPickUp(Player player) {
        Iterator<Item> iter = items.iterator();
        while (iter.hasNext()) {
            Item item = iter.next();
            if (item.overlaps(player)) {
                player.getPowers().add(item.getPower());
                soundHandler.playItemPickUp();
                iter.remove();
            }
        }
    }

    private boolean checkIfItemOverlaps(Item itemToCheck) {
        for (Item item : items) {
            if (item.overlaps(itemToCheck)) {
                return true;
            }
        }
        return false;
    }

    public void drawItems() {
        for (Item item : items) {
            item.draw();
        }
    }

    @SuppressWarnings("unchecked")
    public void checkItemElapsed(float currentTime) {
        if (!player1.getPowers().isEmpty()) {
            ArrayList<Power> playerPowers = (ArrayList<Power>) player1.getPowers().clone();
            for (final Power power : player1.getPowers()) {
                if (power != null && power.getStarted() > 0 && (currentTime >= (power.getStarted() + power.getDuration()))) {
                    if (power.getSelfUse()) {
                        power.removeEffect(player1);
                    } else {
                        power.removeEffect(player2);
                    }
                    playerPowers.remove(power);
                }
            }
            player1.setPowers(playerPowers);
        }
        if (!player2.getPowers().isEmpty()) {
            ArrayList<Power> playerPowers = (ArrayList<Power>) player2.getPowers().clone();
            for (final Power power : player2.getPowers()) {
                if (power != null && power.getStarted() > 0 && (currentTime >= (power.getStarted() + power.getDuration()))) {
                    if (power.getSelfUse()) {
                        power.removeEffect(player2);
                    } else {
                        power.removeEffect(player1);
                    }
                    playerPowers.remove(power);
                }
            }
            player2.setPowers(playerPowers);
        }
    }

    public void resetPlayersPosition() {
        player1.setY(RACQUET_Y);
        player2.setY(RACQUET_Y);
    }

    public void resetScore() {
        player2.setScore(0);
        player1.setScore(0);
    }

    public boolean isInRange(float x, float min, float max) { //if x is between min and max
        return (x >= min && x <= max);
    }
}
