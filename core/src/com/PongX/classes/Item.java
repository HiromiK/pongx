/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.PongX.classes;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;

/**
 *
 * @author Licence
 */
@SuppressWarnings("serial")
public class Item extends Rectangle {

    ShapeRenderer shapeRenderer;
    Power power;

    public Item(float x, float y, float width, float height, Power power) {
        this.setX(x);
        this.setY(y);
        this.setWidth(width);
        this.setHeight(height);
        shapeRenderer = new ShapeRenderer();
        this.power = power;
    }

    public void draw() {
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.rect(this.getX(), this.getY(), this.getWidth(), this.getHeight());
        shapeRenderer.setColor(this.power.getColor());
        shapeRenderer.end();
    }

    public Power getPower() {
        return power;
    }

    public void setPower(Power power) {
        this.power = power;
    }

}
