/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.PongX.classes;

import java.util.ArrayList;

import com.PongX.game.PongX;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;

/**
 *
 * @author Licence
 */
@SuppressWarnings("serial")
public class Player extends Rectangle {

    ShapeRenderer shapeRenderer;
    //Item item;
    ArrayList<Power> powers = new ArrayList<Power>();
    boolean frozen = false;
    int keyUp;
    int keyDown;
    int score;
    int scoreX, scoreY;
    int powerX;
    int powerY;
    BitmapFont bitmapFont;
    SpriteBatch batch;
    int racquetShifting;

    public Player(float x, float y, int up, int down, int scoreX, int scoreY, int powerX, int powerY) {
        this.setX(x);
        this.setY(y);
        this.setWidth(PongX.RACQUET_WIDTH);
        this.setHeight(PongX.RACQUET_HEIGHT);
        this.keyUp = up;
        this.keyDown = down;
        this.score = 0;
        this.scoreX = scoreX;
        this.scoreY = scoreY;
        this.powerX = powerX;
        this.powerY = powerY;
        this.racquetShifting = PongX.RACQUET_SHIFTING;
        this.bitmapFont = new BitmapFont();
        this.batch = new SpriteBatch();
        shapeRenderer = new ShapeRenderer();

    }

    public void draw() {
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.rect(this.getX(), this.getY(), this.getWidth(), this.getHeight());
        shapeRenderer.setColor(Color.WHITE);
        shapeRenderer.end();
        drawScore();
        drawPower();
    }

    public void update() {
        this.setPosition(this.getX(), this.getY());
    }

    public void drawScore() {
        batch.begin();
        this.bitmapFont.setColor(1.0f, 1.0f, 1.0f, 1.0f);
        this.bitmapFont.draw(batch, "" + this.score, this.scoreX, this.scoreY);
        batch.end();
    }

    public void drawPower() {
        if (!this.getPowers().isEmpty()) {
            for (final Power power : this.powers) {
                if (!power.getActive()) {
                    batch.begin();
                    this.bitmapFont.setColor(1.0f, 1.0f, 1.0f, 1.0f);
                    this.bitmapFont.draw(batch, power.getName(), this.powerX, this.powerY);
                    batch.end();
                    break;
                }
            }
        }
    }
    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public ArrayList<Power> getPowers() {
        return powers;
    }

    public void setPowers(ArrayList<Power> powers) {
        this.powers = powers;
    }

    public boolean isFrozen() {
        return frozen;
    }

    public void setFrozen(boolean frozen) {
        this.frozen = frozen;
    }

    public int getKeyUp() {
        return keyUp;
    }

    public void setKeyUp(int keyUp) {
        this.keyUp = keyUp;
    }

    public int getKeyDown() {
        return keyDown;
    }

    public void setKeyDown(int keyDown) {
        this.keyDown = keyDown;
    }

    public int getRacquetShifting() {
        return racquetShifting;
    }

    public void setRacquetShifting(int racquetShifting) {
        this.racquetShifting = racquetShifting;
    }

}
