package com.PongX.classes;

import com.PongX.game.PongX;
import com.badlogic.gdx.graphics.Color;

public abstract class Power implements Cloneable{

    float duration;
    float started;
    Boolean active;
    Boolean selfUse;
    Color color;
    String name;
    int powerX;
    int powerY;
    protected Player caller;

    public abstract void effect(Player player);

    public abstract void removeEffect(Player player);

    public Power(float duration,Boolean selfUse, Color color, String name) {
    	super();
        this.duration = duration;
        this.selfUse = selfUse;
        this.color = color;
        this.name = name;
        this.powerX = 25;
        this.powerY = 25;
        this.active=false;
    }

    public Power() {

    }

    public float getDuration() {
        return duration;
    }

    public void setDuration(float duration) {
        this.duration = duration;
    }

    public float getStarted() {
        return started;
    }

    public void setStarted(float started) {
        this.started = started;
    }

    public Boolean getSelfUse() {
        return selfUse;
    }

    public void setSelfUse(Boolean selfUse) {
        this.selfUse = selfUse;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
    
    public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public Player getCaller() {
		return caller;
	}

	public void setCaller(Player caller) {
		this.caller = caller;
	}

    
	protected void checkBoundsAndAdd(Player player,int heightAdded){
        if (player.getY() + player.getHeight() + (heightAdded / 2) < PongX.SCREEN_HEIGHT && player.getY() - (heightAdded / 2) > 0) {
            player.setHeight(player.getHeight() + heightAdded);
            player.setY(player.getY() - (heightAdded / 2));
        }else if (player.getY() + player.getHeight() + (heightAdded / 2) < PongX.SCREEN_HEIGHT)
            player.setHeight(player.getHeight() + heightAdded);
        else{
            player.setHeight(player.getHeight() + heightAdded);
            player.setY(player.getY() - (heightAdded));
        }
    }
    
    public Power clone() {
		Power o = null;
		try {
			// On r�cup�re l'instance � renvoyer par l'appel de la 
			// m�thode super.clone()
			o = (Power) super.clone();
		} catch(CloneNotSupportedException cnse) {
			// Ne devrait jamais arriver car nous impl�mentons 
			// l'interface Cloneable
			cnse.printStackTrace(System.err);
		}
		// on renvoie le clone
		return o;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPowerX() {
		return powerX;
	}

	public void setPowerX(int powerX) {
		this.powerX = powerX;
	}

	public int getPowerY() {
		return powerY;
	}

	public void setPowerY(int powerY) {
		this.powerY = powerY;
	}
    
    
}
