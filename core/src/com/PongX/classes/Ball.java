package com.PongX.classes;

import com.PongX.game.PongX;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

import java.util.Random;

/**
 *
 * @author Licence
 */
@SuppressWarnings("serial")
public class Ball extends Rectangle {

    public static final float MAX_ACCELERATION = 8;
    public static final Vector2 DIRECTION = new Vector2(100, 100);
    public static final float ACCELERATION = 2;
    // public static final ArrayList<Integer> DIRECTION = new ArrayList<Integer>(Arrays.asList(-100,100));
    //public static final ArrayList<Vector2> DIRECTION = new ArrayList<Vector2>(Arrays.asList(new Vector2(100, 100), new Vector2(-100, 100), new Vector2(100, -100), new Vector2(-100, -100)));

    ShapeRenderer shapeRenderer;
    public Vector2 position;
    public Vector2 velocity;
    float size;
    float acceleration;

    public Ball(float size) {
        this.setWidth(size);
        this.setHeight(size);
        this.setX((PongX.SCREEN_WIDTH / 2) - (this.width / 2));
        this.setY((PongX.SCREEN_HEIGHT / 2) - (this.height / 2));
        this.position = new Vector2(this.x, this.y);
        this.acceleration = ACCELERATION;
        this.velocity = DIRECTION;
        this.size = size;
        this.setRandomDirection();
        shapeRenderer = new ShapeRenderer();
    }

    public void draw() {
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.rect(this.getX(), this.getY(), this.size, this.size);
        shapeRenderer.setColor(Color.WHITE);
        shapeRenderer.end();
    }

    public void update(float deltaTime) {
        position.add(this.velocity.x * deltaTime * this.acceleration, this.velocity.y * deltaTime * this.acceleration);
        this.setPosition(this.position);
    }

    public Vector2 getVelocity() {
        return velocity;
    }

    public void setVelocity(Vector2 velocity) {
        this.velocity = velocity;
    }

    public float getAcceleration() {
        return acceleration;
    }

    public void setAcceleration(float acceleration) {
        if (acceleration <= MAX_ACCELERATION) {
            this.acceleration = acceleration;
        }
    }

    public int outOfScreen() {
        if (this.x + this.width >= PongX.SCREEN_WIDTH) {
            return 2;
        }
        if (this.x <= 0) {
            return 1;
        }
        return 0;
    }

    public void resetPosition() {
        this.setPosition((PongX.SCREEN_WIDTH / 2) - (this.width / 2), (PongX.SCREEN_HEIGHT / 2) - (this.height / 2));
        this.position.set((PongX.SCREEN_WIDTH / 2) - (this.width / 2), (PongX.SCREEN_HEIGHT / 2) - (this.height / 2));
    }

    public void setRandomDirection() {
        this.velocity.scl(random(), random());
    }

    public int random() {
        Random r = new Random();
        if (r.nextBoolean()) {
            return 1;
        } else {
            return -1;
        }
    }

//    public boolean checkMaxSpeed(float acceleration){
//    		return (PongX.isInRange(this.velocity.x * acceleration, 0-BALL_MAXSPEED,BALL_MAXSPEED)) && (PongX.isInRange(this.velocity.y * acceleration, 0-BALL_MAXSPEED,BALL_MAXSPEED));
//    }
}
