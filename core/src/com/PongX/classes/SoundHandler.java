package com.PongX.classes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;

/*


 package com.PongX.classes;

 /**
 *
 * @author Zhongou ZHENG <zhongou.zheng at gmail.com>
 */
public class SoundHandler {

    Sound paddleHit;
    Sound losePoint;
    Sound itemPickUp;
    Sound itemUse;
    Sound gameStart;
    Sound gameOver;
    
    public SoundHandler() {
        paddleHit = Gdx.audio.newSound(Gdx.files.internal("pong_paddle_hit.mp3"));
        losePoint = Gdx.audio.newSound(Gdx.files.internal("lose_point.wav"));
        itemPickUp = Gdx.audio.newSound(Gdx.files.internal("item_pickup.mp3"));
        itemUse = Gdx.audio.newSound(Gdx.files.internal("item_use.wav"));
        gameStart = Gdx.audio.newSound(Gdx.files.internal("game_start.mp3"));
        gameOver = Gdx.audio.newSound(Gdx.files.internal("game_over.wav"));
    }

    public void playPaddleHit() {
        paddleHit.play(0.2f);
    }
    
    public void playLosePoint() {
        losePoint.play(0.5f);
    }
    
    public void playItemPickUp() {
        itemPickUp.play(0.4f);
    }
    
    public void playItemUse() {
        itemUse.play(0.4f);
    }
    
    public void playGameStart() {
        gameStart.play(0.2f);
    }
    
    public void playGameOver() {
        gameOver.play(0.4f);
    }
}
